//
//  ViewController.swift
//  SalaryYota
//
//  Created by d_yumaguzhin on 22.04.2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var employeeSegment: UISegmentedControl!

    @IBOutlet weak var piecesKPIField: UITextField!
    @IBOutlet weak var serviceKPIField: UITextField!
    @IBOutlet weak var productKPIField: UITextField!
    @IBOutlet weak var workingPositionField: UITextField!
    

    @IBOutlet var piecesPlanField: UITextField!
    @IBOutlet var servicePlanField: UITextField!
    @IBOutlet var productPlanField: UITextField!
    
    //Массивы для PickerView
    let positionArray = ["Продавец консультант","Специалист","Старший специалист"]
    
    let valueKPIArray = ["0.10","0.15","0.20","0.25","0.30","0.35","0.40","0.45","0.50","0.55","0.60","0.65","0.70","0.75","0.80","0.85","0.90","0.95","1"]
    
    var positionPickerView = UIPickerView()
    var piecesPickerView = UIPickerView()
    var servicePickerView = UIPickerView()
    var productPickerView = UIPickerView()
 

    override func viewDidLoad() {
        super.viewDidLoad()

        
        positionPickerView.delegate = self
        positionPickerView.dataSource = self
        piecesPickerView.delegate = self
        piecesPickerView.dataSource = self
        servicePickerView.delegate = self
        servicePickerView.dataSource = self
        productPickerView.delegate = self
        productPickerView.dataSource = self
        
        //Подключение PickerView к TextField
        piecesKPIField.inputView = piecesPickerView
        
        //Установка текста по центру поля
        piecesKPIField.textAlignment = .center
        
        piecesKPIField.placeholder = "0.55"
        
        serviceKPIField.inputView = servicePickerView
        serviceKPIField.textAlignment = .center
        serviceKPIField.placeholder = "0.45"
        
        productKPIField.inputView = productPickerView
        productKPIField.textAlignment = .center
        productKPIField.placeholder = "0.10"
        
        productPlanField.textAlignment = .center
        productPlanField.placeholder = "150 296"
        
        piecesPlanField.textAlignment = .center
        piecesPlanField.placeholder = "310"
        
        servicePlanField.textAlignment = .center
        servicePlanField.placeholder = "319"
        
        
        workingPositionField.inputView = positionPickerView
        workingPositionField.textAlignment = .center
        workingPositionField.placeholder = "Специалист"
        
        
        positionPickerView.tag = 1
        piecesPickerView.tag = 2
        servicePickerView.tag = 3
        productPickerView.tag = 4
        
    }


}

extension ViewController:UIPickerViewDataSource {

    //Количество компонентов в pickerView
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //Устанавливается количство строк в PickerView
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 1:
            return positionArray.count
        case 2:
            return valueKPIArray.count
        case 3:
            return valueKPIArray.count
        case 4:
            return valueKPIArray.count
        default:
            return 1
        }
    }


}

extension ViewController:UIPickerViewDelegate {
    
    //Передаем значение в PickerView из массивов
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 1:
            return positionArray[row]
        case 2:
            return valueKPIArray[row]
        case 3:
            return valueKPIArray[row]
        case 4:
            return valueKPIArray[row]
        default:
            return "Data not found."
        }
    }
    //Действия при выборе строги в PickerView
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {

        switch pickerView.tag {
        case 1:
            workingPositionField.text = positionArray[row]
            workingPositionValue = positionArray[row]
            print(workingPositionValue)
            workingPositionField.resignFirstResponder()
        case 2:
            piecesKPIField.text = valueKPIArray[row]
            piecesKPIValue = Double(valueKPIArray[row])!
            print("Штуки \(piecesKPIValue)")
            piecesKPIField.resignFirstResponder()
        case 3:
            serviceKPIField.text = valueKPIArray[row]
            serviceKPIValue = Double(valueKPIArray[row])!
            print("Сервиска \(serviceKPIValue)")
            serviceKPIField.resignFirstResponder()
        case 4:
            productKPIField.text = valueKPIArray[row]
            productKPIValue = Double(valueKPIArray[row])!
            print("Товарка \(productKPIValue)")
            productKPIField.resignFirstResponder()
        default:
            return
        }

    }
    
    //Пытаюсь передать значения в другую View
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destinationVC: InfoViewController = segue.destination as! InfoViewController
        destinationVC.workingPositionText = workingPositionField.text!
    }
    
    
    @IBAction func saveValueKPI(_ sender: AnyObject){
        let text = workingPositionField.text
        workingPositionValue = text!
        
    }
    
    //Выбор значения сегментов
    @IBAction func didChangeSegment(_ sender: UISegmentedControl){
        if sender.selectedSegmentIndex == 0 {
            numberOfEmployees = 1
            print(numberOfEmployees)
        }
        else if sender.selectedSegmentIndex == 1 {
            numberOfEmployees = 2
            print(numberOfEmployees)
        }
        else if sender.selectedSegmentIndex == 2 {
            numberOfEmployees = 3
            print(numberOfEmployees)
        }
        else if sender.selectedSegmentIndex == 3 {
            numberOfEmployees = 4
            print(numberOfEmployees)
        }
        else if sender.selectedSegmentIndex == 4 {
            numberOfEmployees = 5
            print(numberOfEmployees)
        }
        
    }
    //Закрытие клавиатуры при нажати за пределы её области
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
}

